import { TYPES } from './types'

// ShowCaseCards
export function allCars(payload: any) {
    return {
        type: TYPES.ALL_CARS,
        payload,
    }
}

export function carAuto(payload: any) {
    return {
        type: TYPES.CAR_AUTO,
        payload,
    }
}

export function truckAuto(payload: any) {
    return {
        type: TYPES.TRUCK_AUTO,
        payload,
    }
}

// EnterRegister
export function register(payload: any) {
    return {
        type: TYPES.REGISTER,
        payload,
    }
}

export function enter(payload: any) {
    return {
        type: TYPES.ENTER,
        payload,
    }
}

export function exit(payload: any) {
    return {
        type: TYPES.EXIT,
        payload,
    }
}

export function close(payload: any) {
    return {
        type: TYPES.CLOSE,
        payload,
    }
}

export function setUserLogin(payload: any) {
    return {
        type: TYPES.USER_LOGIN,
        payload,
    }
}

export function setUserPassword(payload: any) {
    return {
        type: TYPES.USER_PASSWORD,
        payload,
    }
}

export function clearUser() {
    return {
        type: TYPES.CLEAR_USER,
    }
}

export function clearUserPut() {
    return {
        type: TYPES.CLEAR_USER_PUT,
    }
}

// register
export function setUserNamePut(payload: any) {
    return {
        type: TYPES.USER_NAME_PUT,
        payload,
    }
}

export function setUserLastNamePut(payload: any) {
    return {
        type: TYPES.USER_LASTNAME_PUT,
        payload,
    }
}


export function setUserMailPut(payload: any) {
    return {
        type: TYPES.USER_MAIL_PUT,
        payload,
    }
}

export function setUserLoginPut(payload: any) {
    return {
        type: TYPES.USER_LOGIN_PUT,
        payload,
    }
}

export function setUserPasswordPut(payload: any) {
    return {
        type: TYPES.USER_PASSWORD_PUT,
        payload,
    }
}

export function setUserToken(payload: any) {
    return {
        type: TYPES.SET_USER_TOKEN,
        payload,
    }
}

//windowBuy
export function windowBuy(payload: any) {
    return {
        type: TYPES.WINDOW_BUY,
        payload,
    }
}

export function windowBuyClose(payload: any) {
    return {
        type: TYPES.WINDOW_BUY_CLOSE,
        payload,
    }
}

//setCars
export function setCars(payload: any) {
    return {
        type: TYPES.SET_CARS,
        payload,
    }
}