import { combineReducers } from 'redux'
import Reducer from './reducer'
import RegReducer from './regReducer'

export default () => 
    combineReducers( {
        MAIN: Reducer,
        REGENTER: RegReducer,
    });