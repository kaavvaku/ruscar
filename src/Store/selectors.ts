import { createSelector } from 'reselect'

const getState = (state: any) => state.MAIN;

export const getStateMain = createSelector(getState, (state) => state.cars );