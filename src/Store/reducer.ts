import { TYPES } from './types'
// import { startCars } from '../components/startCars'

// const initialState = {
//     cars: startCars,
// }
const initialState = {
    cars: [],
}


export default function Reducer( state: any = initialState, action: any) {
    // debugger;
    switch(action.type) {
         case TYPES.SET_CARS: 
            return { ...state, cars: action.payload }

        // case TYPES.SET_CARS: 
        //     return { ...state, cars: action.cars }

        // case TYPES.ALL_CARS: 
        //     return { ...state, cars: startCars }

        // case TYPES.CAR_AUTO:
        //     const autoCars: any = startCars.filter( (item: any) => { return item.type === 'car' })
        //     return { ...state, cars: autoCars }

        // case TYPES.TRUCK_AUTO:
        //     const truckCars: any = startCars.filter( (item: any) => { return item.type === 'truck' })
        //     return { ...state, cars: truckCars }

        default: 
            return state
    }
}

// async function makeGetRequest() {
//   let res = await axios.get('http://webcode.me');
//   let data = res.data;
//   console.log(data);
// }
// makeGetRequest();
// debugger;
// async function makeGetRequest() {
//     console.log('2.')
//     let res = await axios.get('http://localhost:9999').then( res => console.log('3.', res.data))
// }
// console.log(`1.`)
// makeGetRequest();

// async function makeGetRequest() {
//     axios
//         .get('http://localhost:9999')
//         .then( response => console.log(`2.`))
// }


// axios
//     .get('http://localhost:9999')
//     // .get('https://social-network.samuraijs.com/api/1.0/users')
//     // .then(response => initialState.cars = response.data.items)
//     .then( response => { })
//     .catch(error => console.log(`error: ${error}`))