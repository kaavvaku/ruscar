import { createSelector } from 'reselect'

const getState = (state: any) => state.REGENTER;

export const stateRegEnter = createSelector(getState, (state) => state.display );

export const stateUserData = createSelector(getState, (state) => state.userData );

export const stateUserDataPut = createSelector(getState, (state) => state.userDataPut );

export const stateUserToken = createSelector(getState, (state) => state.userToken );