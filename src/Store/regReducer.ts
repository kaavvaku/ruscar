import { TYPES } from './types'

const initialState = {
    display: { shadow: 'none', registerWindow: 'none', enterWindow: 'none', windowBuy: 'none'},
    userData: ['', ''],
    userDataPut: ['', '', '', '', ''],
    userToken: ['', '', 'Войти'],
}

export default function RegReducer(state: any = initialState, action: any) {
    
    switch(action.type) {
        case TYPES.USER_LOGIN:
            let newUserLogin = [state.userData[0]+action.payload, state.userData[1]]
            return { ...state, userData : newUserLogin}

        case TYPES.USER_PASSWORD:
            const newUserPassword = [state.userData[0], state.userData[1]+action.payload]
            return { ...state, userData: newUserPassword}

        case TYPES.CLEAR_USER:
            // const clearUser = ['', '']
            return { ...state, userData: ['', '']}

        case TYPES.CLEAR_USER_PUT:
            return { ...state, userDataPut: ['', '', '', '', '']}

        case TYPES.USER_NAME_PUT:
            const newUserNamePut = [ state.userDataPut[0]+action.payload, state.userDataPut[1], state.userDataPut[2], 
                                     state.userDataPut[3], state.userDataPut[4]]
            return { ...state, userDataPut: newUserNamePut}

        case TYPES.USER_LASTNAME_PUT:
            const newUserLastNamePut = [ state.userDataPut[0], state.userDataPut[1]+action.payload, state.userDataPut[2], 
                                     state.userDataPut[3], state.userDataPut[4]]
            return { ...state, userDataPut: newUserLastNamePut}

        case TYPES.USER_MAIL_PUT:
            const newUserMailPut = [ state.userDataPut[0], state.userDataPut[1], state.userDataPut[2]+action.payload, 
                                         state.userDataPut[3], state.userDataPut[4]]
            return { ...state, userDataPut: newUserMailPut}
        
        case TYPES.USER_LOGIN_PUT:
            const newUserLoginPut = [ state.userDataPut[0], state.userDataPut[1], state.userDataPut[2], 
                                     state.userDataPut[3]+action.payload, state.userDataPut[4]]
            return { ...state, userDataPut: newUserLoginPut}
                
        case TYPES.USER_PASSWORD_PUT:
            const newUserPasswordPut = [ state.userDataPut[0], state.userDataPut[1], state.userDataPut[2], 
                                      state.userDataPut[3], state.userDataPut[4]+action.payload]
            return { ...state, userDataPut: newUserPasswordPut}
            
        case TYPES.SET_USER_TOKEN:
            return { ...state, userToken: action.payload}

        case TYPES.REGISTER:
            const newDisplay = {
                    registerWindow: "block",
                    enterWindow: "none",
                    shadow: 'block',
                }
            return { ...state, display: newDisplay}
        
        case TYPES.ENTER: 
            const newDisplayEnter = {
                    enterWindow: "block",
                    registerWindow: "none",
                    shadow: 'block',
                }
            return { ...state, display: newDisplayEnter}

        case TYPES.EXIT:
            return { ...state, userToken: ['', '', 'Войти']}

        case TYPES.CLOSE: 
            const newDisplayClose = {
                    enterWindow: "none",
                    registerWindow: "none",
                    shadow: 'none',
                }
            return { ...state, display: newDisplayClose}

        case TYPES.WINDOW_BUY: 
            const newDisplayBuy = {
                    shadow: 'block',
                    windowBuy: "block",
                }
            return { ...state, display: newDisplayBuy}    

        case TYPES.WINDOW_BUY_CLOSE: 
            const newDisplayBuyClose = {
                    shadow: 'none',
                    windowBuy: "none",
                }
            return { ...state, display: newDisplayBuyClose} 

        default: 
            return state
    }
}