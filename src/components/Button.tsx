function Button( props: any ) {
    return (
        <button className={props.className} onClick={ props.onClick} id={props.id} >
            {/* <a className={props.className}href={props.href}>{props.value}</a> */}
            {props.value}
        </button>
    )
}

export default Button;