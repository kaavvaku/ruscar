import Button from './Button'
import { useDispatch } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { carAuto, truckAuto, allCars } from '../Store/actions'

function LeftMenu() {
    const dispatch: any = useDispatch();
    
    return (
        <div className="classLeftMenu">
            {/* <Button className='classLeftMenuItem' value='Все авто' onClick={ () => dispatch(allCars(null))} /> */}
            {/* <Button className='classLeftMenuItem' value='Легковые' onClick={ () => dispatch(carAuto(null))} /> */}
            {/* <Button className='classLeftMenuItem' value='Грузовые' onClick={ () => dispatch(truckAuto(null))} /> */}
            <NavLink className='classLeftMenuItem' to='/' >Все авто</NavLink>
            <NavLink className='classLeftMenuItem' to='/cars' >Легковые</NavLink>
            <NavLink className='classLeftMenuItem' to='/truck' >Грузовые</NavLink>
        </div>
    )
}

export default LeftMenu;