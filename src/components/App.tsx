import './../App.css';
import Header from './Header/Header'
import NavMenu from './NavMenu'
import Slider from './Slider'
import LeftMenu from './LeftMenu'
import Footer from './Footer/Footer'
import WindowRegEnter from './EnterRegister/WindowRegEnter'
import WindowBuy from './ModalWindows/WindowBuy'
import ShowCase from './MainBlock/ShowCase/ShowCase'
import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom'

function App() {
  return (
    <BrowserRouter>
      <div className='classContainer'>
        <Header />
        <NavMenu />
        <Slider />
        <div className="classMainBlock">
          <LeftMenu />
          <Route exact path='/' render = { () => <ShowCase type={'all'} /> } />
          <Route exact path='/cars' render = { () => <ShowCase type={'car'} /> } />
          <Route exact path='/truck' render = { () => <ShowCase type={'truck'} /> } />
        </div>
        <Footer />
        <WindowRegEnter />
        <WindowBuy />
      </div>
    </BrowserRouter>
    );
}

export default App;
