import { useSelector, useDispatch } from 'react-redux'
import { stateRegEnter } from '../../Store/regSelector'
import { windowBuyClose } from '../../Store/actions'
import Button from './../Button'

function WindowBuy() {
    const styleRegEnter: any = useSelector(stateRegEnter)
    const dispatch: any = useDispatch()

    return (
        <div id="windowBuy" style={{display: `${styleRegEnter.windowBuy}`}} >
            <div className="shadow" style={{display: `${styleRegEnter.shadow}`}} ></div>
            <div className="windowHeader head classActive">Спасибо за покупку</div>
            <Button className="goButton goButtonBuy" id="buyOk" value="Ok" onClick={()=>dispatch(windowBuyClose(null))} />
        </div>
    )
}

export default WindowBuy;