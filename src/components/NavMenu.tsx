import Button from './Button'
import { useSelector, useDispatch } from 'react-redux'
import { enter, exit } from '../Store/actions'
import { stateUserToken } from '../Store/regSelector'
import { NavLink } from 'react-router-dom'

function NavMenu() {
    const userToken: any = useSelector(stateUserToken);
    const dispatch: any = useDispatch();

    const handlerEnterExit = () => {
        if(!userToken[0]) {
            dispatch(enter(null));
        } else {
            dispatch(exit(null))
        }
    }

    return (
        <div className='classNav'>
            <div className='navMenuBox'>
                <NavLink className='classNavMenuItem' to='/' >Все авто</NavLink>
                <NavLink className='classNavMenuItem' to='/cars' >Легковые автомобили</NavLink>
                <NavLink className='classNavMenuItem' to='/truck' >Грузовые автомобили</NavLink>
            </div>
            <Button className='classNavMenuItem classNavMenuUser' value={userToken[1]}/>
            <Button className='classNavMenuItem classNavEnter' value={userToken[2]} onClick={handlerEnterExit} />
        </div>
    )
}

export default NavMenu;