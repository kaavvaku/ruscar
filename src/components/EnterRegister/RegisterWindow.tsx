import { useSelector, useDispatch } from 'react-redux'
import { stateRegEnter, stateUserDataPut } from '../../Store/regSelector'
import { enter, close, setUserNamePut, setUserLastNamePut, setUserMailPut, setUserLoginPut, setUserPasswordPut, setUserToken, clearUserPut } from '../../Store/actions'
import axios from 'axios'

function RegisterWindow() {
    const styleRegEnter: any = useSelector(stateRegEnter);
    const userDataPut: any = useSelector(stateUserDataPut);
    const dispatch: any = useDispatch();

    const handlerInputNamePut = (event: any) => {
        dispatch(setUserNamePut(event.nativeEvent.data))
    }

    const handlerInputLastNamePut = (event: any) => {
        dispatch(setUserLastNamePut(event.nativeEvent.data))
    }

    const handlerInputMailPut = (event: any) => {
        dispatch(setUserMailPut(event.nativeEvent.data))
    }

    const handlerInputLoginPut = (event: any) => {
        dispatch(setUserLoginPut(event.nativeEvent.data))
    }

    const handlerInputPasswordPut = (event: any) => {
        dispatch(setUserPasswordPut(event.nativeEvent.data))
    }
    
    const registerHandler = (event: any) => {
        event.preventDefault();
        axios   
            .post('http://localhost:8888/api/auth/registration', 
                {   'firstName' : userDataPut[0],
                    'lastName' : userDataPut[1],
                    'email' : userDataPut[2],
                    'login' : userDataPut[3],
                    'password' : userDataPut[4]
                })
            .then( res => {
                if(res.data.error) {
                    alert(res.data.error)
                } else {
                    const userTokenName = [userDataPut[2],userDataPut[3], 'Выйти'];
                    dispatch(setUserToken(userTokenName));
                }
            })
            .catch( err => console.log(err));
        dispatch(clearUserPut());
        dispatch(close(null));
    }

    return (
        <div>
            <div id="shadow" style={{display: `${styleRegEnter.shadow}`}}></div>
            <div id="registerWindow" style={{display: `${styleRegEnter.registerWindow}`}}>
                <div className="windowHeader">
                    <div className="head" id="enterReg" onClick={ () => dispatch(enter(null))}>Вход</div>
                    <div className="head">{'\u00A0'}/{'\u00A0'}</div>
                    <div className="head classActive">Регистрация</div>
                </div>
                <form className="formSign" onSubmit={registerHandler} autoComplete="off">
                    <input className="inputName" onChange={handlerInputNamePut} id="inputNameReg" placeholder="Имя..." value={userDataPut[0]}></input>
                    <input className="inputName" autoComplete="new-password" onChange={handlerInputLastNamePut} id="inputMailReg" placeholder="Фамилия..." value={userDataPut[1]}></input>
                    <input className="inputName" autoComplete="new-password" onChange={handlerInputMailPut} id="inputPhoneReg" placeholder="Почта..." value={userDataPut[2]}></input>
                    <input className="inputName" autoComplete="new-password" onChange={handlerInputLoginPut} id="inputPasswordReg" placeholder="Логин..." value={userDataPut[3]}></input>
                    <input className="inputName" autoComplete="new-password" onChange={handlerInputPasswordPut} id="inputPasswordReg" placeholder="Пароль..." value={userDataPut[4]} type='password'></input>
                    <input className="goButton goButtonReg" id="goReg" type="submit" value="Регистрация"></input>
                    <button className="goButton goButtonReg" id="goReg" onClick={()=>dispatch(close(null))}>Отмена</button>
                </form>
            </div>
        </div>
    )
}

export default RegisterWindow;