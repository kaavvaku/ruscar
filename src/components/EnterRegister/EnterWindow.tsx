import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { stateRegEnter, stateUserData } from '../../Store/regSelector'
import { register, close, setUserLogin, setUserPassword, clearUser, setUserToken} from '../../Store/actions'
import axios from 'axios'

function EnterWindow() {
    const styleRegEnter: any = useSelector(stateRegEnter);
    const userData: any = useSelector(stateUserData);
    const dispatch: any = useDispatch()
    
    const handlerInputLogin = (event: any) => {
        dispatch(setUserLogin(event.nativeEvent.data))
    }

    const handlerInputPass = (event: any) => {
        dispatch(setUserPassword(event.nativeEvent.data))
    }

    const enterHandler = (event:any) => {
        event.preventDefault();
        axios
            .post('http://localhost:8888/api/auth/login', 
                {'login': userData[0],
                 'password': userData[1]})
            .then( res => { 
                const userTokenName = [res.data.data.token, userData[0], 'Выйти'];
                dispatch(setUserToken(userTokenName)); })
            .catch( err => {
                alert(`Пользователь \"${userData[0]}\" не зарегистрирован. \nПожалуйста зарегистрируйтесь или проверьте пароль.`)
                console.log('enterWindow error: ', err)
            })
        dispatch(clearUser());
        dispatch(close(null));
    }
    return (
        <div id="enterWindow" style={{display: `${styleRegEnter.enterWindow}`}}>
            <div className="windowHeader">
                <div className="head classActive">Вход</div>
                <div className="head">{'\u00A0'}/{'\u00A0'}</div>
                <div className="head" id="register" onClick={ () => dispatch(register(null))}>Регистрация</div>
            </div>
            <form className="formSign" onSubmit={enterHandler} autoComplete="new-password">
                <input type='text' autoComplete="new-password" className="inputName" onChange={handlerInputLogin} id="inputName" placeholder="Имя *" value={userData[0]}></input>
                <input type='password' className="inputName" onChange={handlerInputPass} id="inputMail" placeholder="Пароль *" value={userData[1]}></input>
                <input className="goButton" id="go" type="submit" value="Вход" ></input>
                <button className="goButton" id="goReg" onClick={()=>dispatch(close(null))} >Отмена</button>
            </form>
            
        </div>
    )
}

export default EnterWindow;