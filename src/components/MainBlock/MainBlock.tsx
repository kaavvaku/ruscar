import LeftMenu from '../LeftMenu'
import ShowCase from './ShowCase/ShowCase'

function MainBlock() {
    return (
        <div className="classMainBlock">
            <LeftMenu />
            <ShowCase type={'all'}/>
        </div>
    )
}

export default MainBlock;
