import { useSelector, useDispatch } from 'react-redux'
import { stateRegEnter } from '../../../Store/regSelector'
import { windowBuy } from '../../../Store/actions'
import Button from '../../Button'

function ShowCaseCard(car) {
    const dispatch = useDispatch()

    const carStyle = {
        backgroundImage: `url(${car.path})`,
    }

    const ratingStyle = {
        backgroundImage: `url(${car.rating})`
    }

    return (
        <div className="showCaseCard" >
            <div className="showCaseCardImg" style={carStyle}></div>
            <div className="showCaseCardInfo">
                <div className="cardInfoBox">
                    <div className="cardInfoBrand">{car.brand}</div>
                    <div className="cardInfoPrice">{car.price}</div>
                    <div className="cardInfoRating" style={ratingStyle}></div>
                </div>
                <Button className='cardButtonBuy' value='Купить' onClick={()=> dispatch(windowBuy(null))} />
            </div>
        </div>
    )
}

export default ShowCaseCard;