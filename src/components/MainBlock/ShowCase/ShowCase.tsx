import { useSelector, useDispatch } from 'react-redux'
import { getStateMain } from '../../../Store/selectors'
import ShowCaseCard from './ShowCaseCard'
import axios from 'axios'
import {setCars } from '../../../Store/actions'

function ShowCase(props:any) {

    const cars: any = useSelector(getStateMain)
    const dispatch: any = useDispatch();

    const handler = (data: any) => { dispatch(setCars(data))} 

    async function makeGetRequest() {
        await axios
            // .get('http://localhost:9999')
            .get('http://localhost:8888/api')
            .then( res => {
                handler(res.data.items)
            })
            .catch( err => console.log('server error: ', err))
    }
    if( cars.length === 0 ) { makeGetRequest(); }
    const trueCar = cars.filter( (elem:any) => props.type === 'all' || elem.type === props.type)

    return (
        <div className="classShowCase">
            {
                trueCar.map( (item: any, index: number) => {
                    return <ShowCaseCard  path={item.path} brand={item.brand} price={item.price} rating={item.rating} key={index}/>
                })
            }
        </div>
    )
}

export default ShowCase;


  