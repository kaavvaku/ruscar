import React from 'react'

// class Slider extends React.Component <{}, {countImage: number}> {
class Slider extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            countImage: 0,
            bgImage: 'classSlider',
        };
    }

    componentDidMount() {
        this.timerID = setInterval( () => this.sliderChange(), 1000*5);
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    sliderChange() {
        let count = this.state.countImage;

        switch(count) {
            case 0:
                this.setState( {countImage: 1, bgImage: 'classSlider classSlider1'});
                break;
            case 1:
                this.setState( {countImage: 2, bgImage: 'classSlider classSlider2'});
                break;
            case 2:
                this.setState( {countImage: 3, bgImage: 'classSlider classSlider3'});
                break;
            case 3:
                this.setState( {countImage: 4, bgImage: 'classSlider classSlider4'});
                break;
            case 4:
                this.setState( {countImage: 0, bgImage: 'classSlider classSlider5'});
                break;
            default: 
                this.setState( { countImage: 0} );
                break;
        }
    }

    render() {
        return (
            <div className={this.state.bgImage}>
                {/* {this.state.countImage} */}
            </div>
        )
    }
}

export default Slider;