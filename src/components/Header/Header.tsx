import HeaderLogo from './HeaderLogo'
import HeaderContacts from './HeaderContacts'

function Header() {
    return (
        <div className='classHeader'>
            <HeaderLogo />
            <HeaderContacts />

        </div>
    )
}

export default Header;