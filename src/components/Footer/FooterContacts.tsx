function FooterContacts() {
    return (
        <div className="classFooterItem">
            <h3>
                Контакты<hr />
            </h3>
            <p className="classFooterContactsText">
                +7 (123) 4-567-890<br /><br />ruscar@mail.ru<br /><br />Нижний Новгород, Кремль, 1
            </p>
            <a className="classFooterLink" href="https://yandex.ru/maps/-/CCUAb4hdpB" target="blank">
                Показать на карте
            </a>
        </div>
    )
}

export default FooterContacts;