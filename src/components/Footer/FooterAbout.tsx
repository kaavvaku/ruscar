function FooterAbout() {

    const footerStyle:any = {
        fontStyle: "italic",
        color: "#09b7f1",
        fontWeight: 700,
        textTransform: "uppercase",
    }

    return (
        <div className="classFooterItem">
            <h3 className="h3">О нас<hr /></h3>
            <a className="classFooterLink" href='null'>
                Служба качества
            </a>
            <a className="classFooterLink" href="null">
                <br />Новости компании
            </a>
            <a className="classFooterLink" href="null">
                <br />Работа в <span style={footerStyle}>ruscar</span>
            </a>
        </div>
    )
}

export default FooterAbout;