import FooterAbout from './FooterAbout'
import FooterContacts from './FooterContacts'

function Footer() {
    return (
        <div className="classFooter">
            <div className="classFooterLogo"></div>
            <FooterAbout />
            <FooterContacts />
        </div>
        
    )
}

export default Footer;